# ultimate-tic-tac-toe

The normal game of tic-tac-toe is very simple and very easy to master. At peak performance, both player are basically navigating through a [flowchart](https://xkcd.com/832/) and ending the game in stalemate after stalemate. Ultimate Tic-Tac-Toe aims to solve this by upping the scale of the game; you are not playing on a single three by three grid, but instead playing on a larger three by three grid made up of nine smaller three by three grids.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
