module.exports = {
	checkForWin: function(grid, symbol) {
		return checkHorizontals(grid, symbol) || checkVerticals(grid, symbol) || checkDiagonals(grid, symbol)
	},
}

function checkHorizontals(grid, symbol) {
	for (let i = 0; i < grid.length; i++) {
		const row = grid[i]
		let line = true
		row.forEach((tile) => {
			if (tile.symbol !== symbol) line = false
		})
		if (line) return line
	}
}

function checkVerticals(grid, symbol) {
	return (
		(grid[0][0].symbol === symbol && grid[1][0].symbol === symbol && grid[2][0].symbol === symbol) ||
		(grid[0][1].symbol === symbol && grid[1][1].symbol === symbol && grid[2][1].symbol === symbol) ||
		(grid[0][2].symbol === symbol && grid[1][2].symbol === symbol && grid[2][2].symbol === symbol)
	)
}

function checkDiagonals(grid, symbol) {
	return (
		(grid[0][0].symbol === symbol && grid[1][1].symbol === symbol && grid[2][2].symbol === symbol) ||
		(grid[0][2].symbol === symbol && grid[1][1].symbol === symbol && grid[2][0].symbol === symbol)
	)
}
