module.exports = {
	isGridFull: function(innerGrid) {
		if (innerGrid.symbol) return true
		for (let i = 0; i < innerGrid.grid.length; i++) {
			for (let iter = 0; iter < innerGrid.grid[i].length; iter++) {
				if (!innerGrid.grid[i][iter].symbol) return false
			}
		}
		return true
	},
}
