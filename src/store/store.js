import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		player: '❌',
		finished: false,
		grid: [
			[
				{
					symbol: '',
					clickable: true,
					grid: [
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
					],
				},
				{
					symbol: '',
					clickable: true,
					grid: [
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
					],
				},
				{
					symbol: '',
					clickable: true,
					grid: [
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
					],
				},
			],
			[
				{
					symbol: '',
					clickable: true,
					grid: [
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
					],
				},
				{
					symbol: '',
					clickable: true,
					grid: [
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
					],
				},
				{
					symbol: '',
					clickable: true,
					grid: [
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
					],
				},
			],
			[
				{
					symbol: '',
					clickable: true,
					grid: [
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
					],
				},
				{
					symbol: '',
					clickable: true,
					grid: [
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
					],
				},
				{
					symbol: '',
					clickable: true,
					grid: [
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
						[{ symbol: '' }, { symbol: '' }, { symbol: '' }],
					],
				},
			],
		],
	},
	mutations: {
		captureTile(state, position) {
			Vue.set(
				state.grid[position.outerRow][position.outerColumn].grid[position.innerRow][position.innerColumn],
				'symbol',
				state.player
			)
		},
		captureGrid(state, position) {
			Vue.set(state.grid[position.outerRow][position.outerColumn], 'symbol', state.player)
		},
		makeOneGridClickable(state, { innerRow: nextGridRow, innerColumn: nextGridColumn }) {
			for (let i = 0; i < state.grid.length; i++) {
				for (let iter = 0; iter < state.grid[i].length; iter++) {
					if (i === nextGridRow && iter === nextGridColumn) Vue.set(state.grid[i][iter], 'clickable', true)
					else Vue.set(state.grid[i][iter], 'clickable', false)
				}
			}
		},
		makeAllGridsClickable(state) {
			for (let i = 0; i < state.grid.length; i++) {
				for (let iter = 0; iter < state.grid[i].length; iter++) {
					Vue.set(state.grid[i][iter], 'clickable', true)
				}
			}
		},
		win(state) {
			state.finished = true
		},
		switchPlayers(state) {
			state.player = state.player === '❌' ? '⭕' : '❌'
		},
	},
	getters: {
		getGrid: (state) => {
			return state.grid
		},
	},
})
